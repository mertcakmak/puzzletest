"use strict"
const express = require('express');
const api = express();
const waterfall = require('async-waterfall');
const moment = require('moment');
const puzzleSetups = require('./PuzzleSetups.json');

api.use(express.urlencoded({extended:true}));
api.use(express.json());

api.use((req,res,next)=>{
    res.status(200);
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");  
    next();
})

api.get("/api/getPuzzleDetails/:puzzleName?",(req,res)=>{
    const puzzleName = req.query.puzzleName || "puzzle1";
    res.send(puzzleSetups.puzzles.find((item)=>item.name.toLowerCase()===puzzleName.toLowerCase()));
});

api.post("/api/getThomasMoveOptions",(req,res)=>{
    let rs = {data:{},error:null,duration:0};
    const startTime = moment(new Date().getTime());

    const puzzleName = req.body.puzzleName || "puzzle1";
    const thomas = req.body.thomas;

    waterfall([
        async(cb)=>{
            const selectedPuzzle = puzzleSetups.puzzles.find((item)=>item.name.toLowerCase()===puzzleName.toLowerCase());
            if(selectedPuzzle){
                cb(null,selectedPuzzle,thomas);
            } else {
                cb('No records found for '+puzzleName+' ',{});
            }
        }
        ,async(selectedPuzzle,thomas,cb)=>{
            const currentThomas = findFromLayout(selectedPuzzle.layout,thomas.row,thomas.column);
            let thomasMoveOptions = {};
            thomasMoveOptions.top = currentThomas.borders.indexOf("T") && findFromLayout(selectedPuzzle.layout,parseInt(thomas.row)-1,parseInt(thomas.column));
            thomasMoveOptions.right = currentThomas.borders.indexOf("R") && findFromLayout(selectedPuzzle.layout,parseInt(thomas.row),parseInt(thomas.column)+1);
            thomasMoveOptions.bottom = currentThomas.borders.indexOf("B") && findFromLayout(selectedPuzzle.layout,parseInt(thomas.row)+1,parseInt(thomas.column));
            thomasMoveOptions.left = currentThomas.borders.indexOf("L") && findFromLayout(selectedPuzzle.layout,parseInt(thomas.row),parseInt(thomas.column)-1);
            cb(null,thomasMoveOptions);
        }
        
    ],async(error,data)=>{
        const endTime = moment(new Date().getTime());
        const duration = (endTime-startTime)/1000;

        rs.error = error;
        rs.data = data;
        rs.duration = duration
        res.send(rs);
    })
});

api.post("/api/checkWolfMovement",(req,res)=>{
    let rs = {data:{},error:null,duration:0};
    const startTime = moment(new Date().getTime());

    const puzzleName = req.body.puzzleName || "puzzle1";
    const thomas = req.body.thomas;
    const wolf = req.body.wolf;

    waterfall([
        async(cb)=>{
            const selectedPuzzle = puzzleSetups.puzzles.find((item)=>item.name.toLowerCase()===puzzleName.toLowerCase());
            if(selectedPuzzle){
                cb(null,selectedPuzzle,thomas,wolf);
            } else {
                cb('No records found for '+puzzleName+' ',{});
            }
        }
        ,async(selectedPuzzle,thomas,wolf,cb)=>{
            wolf = parseInt(thomas.row)===parseInt(wolf.row) ? wolfMoveHorizontal(selectedPuzzle.layout,thomas,wolf) : wolfMoveVertical(selectedPuzzle.layout,thomas,wolf);
            cb(null,selectedPuzzle,thomas,wolf);
        }
        ,async(selectedPuzzle,thomas,wolf,cb)=>{
            wolf = parseInt(thomas.row)===parseInt(wolf.row) ? wolfMoveHorizontal(selectedPuzzle.layout,thomas,wolf) : wolfMoveVertical(selectedPuzzle.layout,thomas,wolf);
            cb(null,wolf);
        }
    ],async(error,data)=>{
        const endTime = moment(new Date().getTime());
        const duration = (endTime-startTime)/1000;

        rs.error = error;
        rs.data = data;
        rs.duration = duration
        res.send(rs);
    });
});


const findFromLayout = (layout,row,column)=>{
    const rs = layout.find((item)=> (item.row===parseInt(row) && item.column===parseInt(column)) );
    return(rs);
}

const wolfMoveHorizontal = (layout,thomas, wolf)=>{
    const currentWolf = findFromLayout(layout,wolf.row,wolf.column);
    if(wolf.column > thomas.column){
        wolf.column = currentWolf.borders.indexOf("L")===-1 ? parseInt(wolf.column)-1 : wolf.column;
    } else {
        wolf.column = currentWolf.borders.indexOf("R")==-1 ? parseInt(wolf.column)+1 : wolf.column;
    }
    return(wolf);

}

const wolfMoveVertical = (layout,thomas, wolf)=>{
    const currentWolf = findFromLayout(layout,wolf.row,wolf.column);
    if(wolf.row > thomas.row){
        wolf.row = currentWolf.borders.indexOf("T")===-1 ? parseInt(wolf.row)-1 : wolf.row;
    } else {
        wolf.row = currentWolf.borders.indexOf("B")===-1 ? parseInt(wolf.row)+1 : wolf.row;
    }
    return(wolf);
}

api.listen(3000,()=>{console.log("puzzle api started")});