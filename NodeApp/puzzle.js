const puzzle = (function(){
    const serverUrl = "http://localhost:3000";
    let currentPuzzle = {};
    let limit = 0;
    let thomasMoveOptions = {};

    const pickPuzzle = (puzzleName)=>{
        jQuery.ajax({
            type:"GET",
            url:serverUrl+'/api/getPuzzleDetails?puzzleName='+puzzleName,
            success:(q)=>{
                currentPuzzle = q;
                initPuzzle();
            }
        })
    }

    const initPuzzle = ()=>{
        jQuery('#puzzleContainer').html('');
        limit = Math.sqrt(currentPuzzle.layout.length);

        jQuery(currentPuzzle.layout).each((q,w)=>{
            if(q%limit==0){
                const puzzleRow = '<div class="puzzleRow d-flex" data-row="'+w.row+'"></div>';
                jQuery('#puzzleContainer').append(puzzleRow);
            }

            const arr_borders = w.borders.split('');
            let classVal = 'puzzleColumn  d-flex justify-content-center align-items-center ';
            let dataWallTop = 0;
            let dataWallLeft = 0;
            let dataWallBottom = 0;
            let dataWallRight = 0;
            arr_borders.forEach((borderPosition)=>{
                switch(borderPosition){
                    case "T": classVal += ' border-top '; dataWallTop = 1; break;
                    case "R": classVal += ' border-right '; dataWallRight = 1; break;
                    case "B": classVal += ' border-bottom '; dataWallBottom = 1; break;
                    case "L": classVal += ' border-left '; dataWallLeft = 1; break;
                }
            })

            const titleVal = String(w.row)+String(w.column);
            const puzzleColumn = '<div class="'+classVal+'" data-row="'+w.row+'" data-column="'+w.column+'" '
            +' data-wall-top="'+dataWallTop+'" data-wall-right="'+dataWallRight+'" data-wall-bottom="'+dataWallBottom+'" data-wall-left="'+dataWallLeft+'">'
            +titleVal+'</div>';
            jQuery('.puzzleRow[data-row="'+w.row+'"]').append(puzzleColumn);
        });
        moveUser('thomas');
        moveUser('wolf');
        findMoveOptions();
        initThomasMoveButton();
    }

    const moveUser = (userType) =>{
        const bgColor = userType==='thomas' ? 'bg-success' : 'bg-danger';

        jQuery('.puzzleColumn[data-user-type="'+userType+'"]').removeClass(bgColor);
        jQuery('.puzzleColumn[data-user-type="'+userType+'"]').attr('data-user-type','');

        jQuery('.puzzleColumn[data-user-type="'+userType+'"]').attr('data-user-type','');
        jQuery('.puzzleColumn[data-row="'+currentPuzzle[userType].row+'"][data-column="'+currentPuzzle[userType].column+'"]').addClass(bgColor);
        jQuery('.puzzleColumn[data-row="'+currentPuzzle[userType].row+'"][data-column="'+currentPuzzle[userType].column+'"]').attr('data-user-type',userType);
        checkThomasPosition();
    }

    const checkThomasPosition = ()=>{
        if(!jQuery('.puzzleColumn[data-user-type="thomas"]').length){
            gameOver(true);
            return false;
        }
    }

    const findMoveOptions = ()=>{
        const postData = { 
            puzzleName:currentPuzzle.name,
            thomas:currentPuzzle.thomas
        }

        jQuery.ajax({
            type:"POST",
            data:postData,
            dataType:"JSON",
            url:serverUrl+'/api/getThomasMoveOptions',
            success:(q)=>{
                if(q.error){
                    alert(q.error);
                    return false;
                } 
                thomasMoveOptions = q.data;
            }
            ,complete:()=>{
                checkMoveButton();
            }
        })
    }

    const checkMoveButton = ()=>{
        jQuery('button.thomasMove').each((q,w)=>{
            const move = jQuery(w).attr('data-move');
            jQuery(w).find('span').html('-');    
            jQuery(w).removeClass('btn-primary');
            jQuery(w).attr('disabled','disabled');
            if(thomasMoveOptions[move]){ 
                jQuery(w).addClass('btn-primary');
                jQuery(w).removeAttr('disabled');
                jQuery(w).find('span').html(String(thomasMoveOptions[move].row)+String(thomasMoveOptions[move].column));
            }
        })
    }

    const checkWolfMovement = ()=>{
        const postData = {
            puzzleName:currentPuzzle.name,
            thomas:currentPuzzle.thomas,
            wolf:currentPuzzle.wolf
        }

        jQuery.ajax({
            type:"POST",
            data:postData,
            dataType:"JSON",
            url:serverUrl+'/api/checkWolfMovement',
            success:(q)=>{
                if(q.error){
                    alert(q.error);
                    return false;
                } 
                currentPuzzle.wolf = q.data;
            }
            ,complete:()=>{
                moveUser('wolf');
            }
        })
    }

    const wolfMoveHorizontal = (n)=>{
        let columnValue = currentPuzzle.wolf.column;
        let enableMove = false;
        if(currentPuzzle.wolf.column > currentPuzzle.thomas.column){
            enableMove  = jQuery('.puzzleColumn[data-user-type="wolf"]').attr('data-wall-left')==="0";
            columnValue--;
        } else {
            enableMove  = jQuery('.puzzleColumn[data-user-type="wolf"]').attr('data-wall-right')==="0";
            columnValue++;
        }

        const wolfMove = {row:currentPuzzle.wolf.row,column:columnValue};
        if(enableMove){
            currentPuzzle.wolf = wolfMove;
            moveUser('wolf');
        }
        checkWolfMovement(n-1);
    }

    const wolfMoveVertical = (n)=>{
        let rowValue = currentPuzzle.wolf.row;
        let enableMove = false;
        if(currentPuzzle.wolf.row > currentPuzzle.thomas.row){
            enableMove  = jQuery('.puzzleColumn[data-user-type="wolf"]').attr('data-wall-top')==="0";
            rowValue--;
        } else {
            enableMove  = jQuery('.puzzleColumn[data-user-type="wolf"]').attr('data-wall-bottom')==="0";
            rowValue++;
        }

        const wolfMove = {row:rowValue,column:currentPuzzle.wolf.column};
        if(enableMove){
            currentPuzzle.wolf = wolfMove;
            moveUser('wolf');
        }
        checkWolfMovement(n-1);
    }

    const initThomasMoveButton = ()=>{
        jQuery('button.thomasMove').click((e)=>{
            const move = jQuery(e.target).attr("data-move");
            if(thomasMoveOptions[move]){                 
                currentPuzzle.thomas = thomasMoveOptions[move]; 
                moveUser('thomas');
                checkWolfMovement();
                findMoveOptions();
            }
        });

        jQuery(document).keydown((e)=>{
            switch(e.which){
                case 38 : jQuery('button.thomasMove[data-move="top"]').trigger('click'); break;
                case 39 : jQuery('button.thomasMove[data-move="right"]').trigger('click'); break;
                case 40 : jQuery('button.thomasMove[data-move="bottom"]').trigger('click'); break;
                case 37 : jQuery('button.thomasMove[data-move="left"]').trigger('click'); break;
            }
        });
    }

    const gameOver = (status)=>{
        switch(status){
            case true :
                jQuery('#gameOverModal .modal-title').html('Success!');
                jQuery('#gameOverModal .modal-body').html('Thomas successfuly escaped!');
            break;
            case false:
                jQuery('#gameOverModal .modal-title').html('Failed!');
                jQuery('#gameOverModal .modal-body').html('Thomas was catched by wolf!');
            break;
        }
        jQuery('#gameOverModal').modal('show');
    }

    return {
        init:(puzzleName="puzzle1")=>{
            jQuery('#gameOverModal').modal('hide');
            pickPuzzle(puzzleName);
        }
    }
})();
