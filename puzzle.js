const puzzle = (function(){
    let currentPuzzle = {};
    let limit = 0;
    let thomasMoveOptions = {};

    const pickPuzzle = (puzzleName)=>{
        const selectedPuzzle = puzzleSetups.puzzles.find((item)=>item.name===puzzleName);
        if(selectedPuzzle){
            currentPuzzle = selectedPuzzle;
            initPuzzle();
        }
    }

    const initPuzzle = ()=>{
        jQuery('#puzzleContainer').html('');
        limit = Math.sqrt(currentPuzzle.layout.length);

        jQuery(currentPuzzle.layout).each((q,w)=>{
            if(q%limit==0){
                const puzzleRow = '<div class="puzzleRow d-flex" data-row="'+w.row+'"></div>';
                jQuery('#puzzleContainer').append(puzzleRow);
            }

            const arr_borders = w.borders.split('');
            let classVal = 'puzzleColumn  d-flex justify-content-center align-items-center ';
            let dataWallTop = 0;
            let dataWallLeft = 0;
            let dataWallBottom = 0;
            let dataWallRight = 0;
            arr_borders.forEach((borderPosition)=>{
                switch(borderPosition){
                    case "T": classVal += ' border-top '; dataWallTop = 1; break;
                    case "R": classVal += ' border-right '; dataWallRight = 1; break;
                    case "B": classVal += ' border-bottom '; dataWallBottom = 1; break;
                    case "L": classVal += ' border-left '; dataWallLeft = 1; break;
                }
            })

            const titleVal = String(w.row)+String(w.column);
            const puzzleColumn = '<div class="'+classVal+'" data-row="'+w.row+'" data-column="'+w.column+'" '
            +' data-wall-top="'+dataWallTop+'" data-wall-right="'+dataWallRight+'" data-wall-bottom="'+dataWallBottom+'" data-wall-left="'+dataWallLeft+'">'
            +titleVal+'</div>';
            jQuery('.puzzleRow[data-row="'+w.row+'"]').append(puzzleColumn);
        });
        moveUser('thomas');
        moveUser('wolf');
        findMoveOptions();
        initThomasMoveButton();
    }

    const moveUser = (userType) =>{
        const bgColor = userType==='thomas' ? 'bg-success' : 'bg-danger';

        jQuery('.puzzleColumn[data-user-type="'+userType+'"]').removeClass(bgColor);
        jQuery('.puzzleColumn[data-user-type="'+userType+'"]').attr('data-user-type','');

        jQuery('.puzzleColumn[data-user-type="'+userType+'"]').attr('data-user-type','');
        jQuery('.puzzleColumn[data-row="'+currentPuzzle[userType].row+'"][data-column="'+currentPuzzle[userType].column+'"]').addClass(bgColor);
        jQuery('.puzzleColumn[data-row="'+currentPuzzle[userType].row+'"][data-column="'+currentPuzzle[userType].column+'"]').attr('data-user-type',userType);
        checkThomasPosition();
    }

    const checkThomasPosition = ()=>{
        if(!jQuery('.puzzleColumn[data-user-type="thomas"]').length){
            gameOver(true);
            return false;
        }
    }

    const findMoveOptions = ()=>{
        let top = jQuery('.puzzleColumn[data-user-type="thomas"]').attr('data-wall-top')==="1" ? 0 : currentPuzzle.thomas.row-1;
        let right = jQuery('.puzzleColumn[data-user-type="thomas"]').attr('data-wall-right')==="1" ? 0 : currentPuzzle.thomas.column+1;
        let bottom = jQuery('.puzzleColumn[data-user-type="thomas"]').attr('data-wall-bottom')==="1" ? 0 : currentPuzzle.thomas.row+1;
        let left = jQuery('.puzzleColumn[data-user-type="thomas"]').attr('data-wall-left')==="1" ? 0 : currentPuzzle.thomas.column-1;

        thomasMoveOptions.top = top !== 0 && { row : top, column : currentPuzzle.thomas.column }; checkMoveButton('top');
        thomasMoveOptions.right = right !== 0 && { row : currentPuzzle.thomas.row, column : right }; checkMoveButton('right');
        thomasMoveOptions.bottom = bottom !== 0 && { row : bottom, column : currentPuzzle.thomas.column }; checkMoveButton('bottom');
        thomasMoveOptions.left = left !== 0 && { row : currentPuzzle.thomas.row, column : left}; checkMoveButton('left');
    }

    const checkMoveButton = (move)=>{
        jQuery('button.thomasMove[data-move="'+move+'"] span').html('-');
        jQuery('button.thomasMove[data-move="'+move+'"]').removeClass('btn-primary');
        jQuery('button.thomasMove[data-move="'+move+'"]').attr('disabled','disabled');
        if(thomasMoveOptions[move]){ 
            jQuery('button.thomasMove[data-move="'+move+'"]').addClass('btn-primary');
            jQuery('button.thomasMove[data-move="'+move+'"]').removeAttr('disabled');
            jQuery('button.thomasMove[data-move="'+move+'"] span').html(String(thomasMoveOptions[move].row)+String(thomasMoveOptions[move].column));
        }
    }

    const checkWolfMovement = (n=2)=>{
        if(currentPuzzle.thomas.row==currentPuzzle.wolf.row && currentPuzzle.thomas.column==currentPuzzle.wolf.column) {  
            gameOver(false);
            return false;
        }
        
        if(n===0) return false;
        currentPuzzle.thomas.row === currentPuzzle.wolf.row ? wolfMoveHorizontal(n) : wolfMoveVertical(n);
    }

    const wolfMoveHorizontal = (n)=>{
        let columnValue = currentPuzzle.wolf.column;
        let enableMove = false;
        if(currentPuzzle.wolf.column > currentPuzzle.thomas.column){
            enableMove  = jQuery('.puzzleColumn[data-user-type="wolf"]').attr('data-wall-left')==="0";
            columnValue--;
        } else {
            enableMove  = jQuery('.puzzleColumn[data-user-type="wolf"]').attr('data-wall-right')==="0";
            columnValue++;
        }

        const wolfMove = {row:currentPuzzle.wolf.row,column:columnValue};
        if(enableMove){
            currentPuzzle.wolf = wolfMove;
            moveUser('wolf');
        }
        checkWolfMovement(n-1);
    }

    const wolfMoveVertical = (n)=>{
        let rowValue = currentPuzzle.wolf.row;
        let enableMove = false;
        if(currentPuzzle.wolf.row > currentPuzzle.thomas.row){
            enableMove  = jQuery('.puzzleColumn[data-user-type="wolf"]').attr('data-wall-top')==="0";
            rowValue--;
        } else {
            enableMove  = jQuery('.puzzleColumn[data-user-type="wolf"]').attr('data-wall-bottom')==="0";
            rowValue++;
        }

        const wolfMove = {row:rowValue,column:currentPuzzle.wolf.column};
        if(enableMove){
            currentPuzzle.wolf = wolfMove;
            moveUser('wolf');
        }
        
        checkWolfMovement(n-1);
    }

    const initThomasMoveButton = ()=>{
        jQuery('button.thomasMove').click((e)=>{
            const move = jQuery(e.target).attr("data-move");
            if(thomasMoveOptions[move]){                 
                currentPuzzle.thomas = thomasMoveOptions[move]; 
                moveUser('thomas');
                checkWolfMovement();
                findMoveOptions();
            }
        });

        jQuery(document).keydown((e)=>{
            switch(e.which){
                case 38 : jQuery('button.thomasMove[data-move="top"]').trigger('click'); break;
                case 39 : jQuery('button.thomasMove[data-move="right"]').trigger('click'); break;
                case 40 : jQuery('button.thomasMove[data-move="bottom"]').trigger('click'); break;
                case 37 : jQuery('button.thomasMove[data-move="left"]').trigger('click'); break;
            }
        });
    }

    const gameOver = (status)=>{
        switch(status){
            case true :
                jQuery('#gameOverModal .modal-title').html('Success!');
                jQuery('#gameOverModal .modal-body').html('Thomas successfuly escaped!');
            break;
            case false:
                jQuery('#gameOverModal .modal-title').html('Failed!');
                jQuery('#gameOverModal .modal-body').html('Thomas was catched by wolf!');
            break;
        }
        jQuery('#gameOverModal').modal('show');
    }

    return {
        init:(puzzleName="puzzle1")=>{
            jQuery('#gameOverModal').modal('hide');
            pickPuzzle(puzzleName);
        }
    }
})();

const puzzleSetups = {
    "puzzles": [
        {
            "name": "puzzle1",
            "layout": [
                { "row": 1, "column": 1, "borders": "LT" },
                { "row": 1, "column": 2, "borders": "TB" },
                { "row": 1, "column": 3, "borders": "TB" },
                { "row": 1, "column": 4, "borders": "T" },
                { "row": 1, "column": 5, "borders": "T" },
                { "row": 1, "column": 6, "borders": "TB" },
                { "row": 1, "column": 7, "borders": "TR" },
                { "row": 2, "column": 1, "borders": "LR" },
                { "row": 2, "column": 2, "borders": "LTB" },
                { "row": 2, "column": 3, "borders": "TB" },
                { "row": 2, "column": 4, "borders": "R" },
                { "row": 2, "column": 5, "borders": "L" },
                { "row": 2, "column": 6, "borders": "TR" },
                { "row": 2, "column": 7, "borders": "LR" },
                { "row": 3, "column": 1, "borders": "LR" },
                { "row": 3, "column": 2, "borders": "LT" },
                { "row": 3, "column": 3, "borders": "T" },
                { "row": 3, "column": 4, "borders": "R" },
                { "row": 3, "column": 5, "borders": "LR" },
                { "row": 3, "column": 6, "borders": "LR" },
                { "row": 3, "column": 7, "borders": "LR" },
                { "row": 4, "column": 1, "borders": "LR" },
                { "row": 4, "column": 2, "borders": "L" },
                { "row": 4, "column": 3, "borders": "" },
                { "row": 4, "column": 4, "borders": "B" },
                { "row": 4, "column": 5, "borders": "R" },
                { "row": 4, "column": 6, "borders": "LR" },
                { "row": 4, "column": 7, "borders": "LR" },
                { "row": 5, "column": 1, "borders": "LR" },
                { "row": 5, "column": 2, "borders": "LR" },
                { "row": 5, "column": 3, "borders": "LB" },
                { "row": 5, "column": 4, "borders": "T" },
                { "row": 5, "column": 5, "borders": "R" },
                { "row": 5, "column": 6, "borders": "LR" },
                { "row": 5, "column": 7, "borders": "LR" },
                { "row": 6, "column": 1, "borders": "LR" },
                { "row": 6, "column": 2, "borders": "LR" },
                { "row": 6, "column": 3, "borders": "LTB" },
                { "row": 6, "column": 4, "borders": "B" },
                { "row": 6, "column": 5, "borders": "BR" },
                { "row": 6, "column": 6, "borders": "LR" },
                { "row": 6, "column": 7, "borders": "LR" },
                { "row": 7, "column": 1, "borders": "LR" },
                { "row": 7, "column": 2, "borders": "LB" },
                { "row": 7, "column": 3, "borders": "TB" },
                { "row": 7, "column": 4, "borders": "TB" },
                { "row": 7, "column": 5, "borders": "TB" },
                { "row": 7, "column": 6, "borders": "B" },
                { "row": 7, "column": 7, "borders": "BR" }
            ],
            "wolf": { "row": 3, "column": 6 },
            "thomas": { "row": 3, "column": 4 } 
        }
    ]
};
